#!/usr/bin/python
# -*- coding: UTF-8 -*-

if __name__ == '__main__':
    tmax = 0
    # Amount of calories for the elves
    calories: list[int] = []
    # Current amount
    current_amount: int = 0
    with open('input.txt', encoding='utf-8') as input_file:
        # Retrieve the list of each elf's inventory
        elves_inputs: list[str] = input_file.read().strip().split('\n\n')
        # Split the inventory into list of items
        elves_lists: list[list[str]] = [
            elves_input.split('\n') for elves_input in elves_inputs]
        # Convert items to ints and sum them
        snack_values: list[int] = [
            sum([int(snack_cal) for snack_cal in snack_list])
            for snack_list in elves_lists
        ]

    # Part 1
    # print(max(snack_values))

    # Part 2
    pt2: int = sum(sorted(snack_values, reverse=True)[0:3])
    print(pt2)
