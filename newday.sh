#!/bin/bash

# Create the directory structure for the given day

mkdir -p $1/src $1/data
touch $1/README.md
