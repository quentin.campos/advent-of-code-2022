from string import ascii_lowercase, ascii_uppercase
from rucksack import RuckSack
from functools import lru_cache


@lru_cache(maxsize=26*2)
def score(item: str) -> int:
    """ Return the score of the given item """
    if item in ascii_lowercase:
        return ord(item) - ord('a') + 1
    if item in ascii_uppercase:
        return ord(item) - ord('A') + 1 + 26


def main():
    # Sum of the common items priorities
    priorities: int = 0
    # Parse input file
    with open("../data/input.txt", encoding="utf-8") as input_file:
        for line in input_file:
            r: RuckSack = RuckSack.parse(line)
            for item in r.common():
                priorities += score(item)

    print(priorities)


if __name__ == '__main__':
    main()
