from functools import reduce
from part1 import score


def group_token(group):
    assert(len(group) == 3)


def iter_chunk(n: int, iterable):
    group = []
    while True:
        try:
            group.append(next(iterable)[:-1])
        except StopIteration:
            if group:
                yield group
            return
        if len(group) == n:
            yield group
            group = []


def set_intersect(a: set, b: set):
    return a.intersection(b)


def main():
    # Sum of the common items priorities
    priorities: int = 0
    # Parse input file
    with open("data/input.txt", encoding="utf-8") as input_file:
        for trio in iter_chunk(3, input_file):
            trio = list(trio)
            trio = map(set, trio)
            items = reduce(set_intersect, trio)
            priorities += score(list(items)[0])

    print(priorities)


if __name__ == '__main__':
    main()
