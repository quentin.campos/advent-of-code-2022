#!/usr/bin/python
# -*- coding: UTF-8 -*-

from dataclasses import dataclass
from collections import Counter


@dataclass
class RuckSack:
    _left: str
    _right: str

    @classmethod
    def parse(cls, content):
        content = content.strip()
        size = len(content)
        return RuckSack(
            content[0: size//2],
            content[size//2:]
        )

    def left(self) -> set[str]:
        return Counter(self._left)

    def right(self) -> set[str]:
        return Counter(self._right)

    def common(self):
        """ Return the items in common between the two spaces of the rucksack.
        """
        return set(self.left()).intersection(self.right())
