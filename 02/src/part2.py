#!/usr/bin/python
# -*- coding: UTF-8 -*-
from functools import lru_cache


@lru_cache(maxsize=9)
def line_score(line: str) -> int:
    """ Return the score given obtained from the given line.

    Since there is a small and limited number of possibilities, results are
    cached.

    Results could be computed by hand beforehand and added to a map
    line -> score. But this is christmas fun.
    """
    # Bonus for the move selected
    bonus_score: dict = {'A': 1, 'B': 2, 'C': 3}
    # Parse players moves
    opponent: str = line[0]
    expected_output: str = line[2]
    # Check match result
    match_score = {
        # Rock
        'A': {'X': 0 + bonus_score['C'], 'Y': 3 + bonus_score['A'], 'Z': 6 + bonus_score['B']},
        # Paper
        'B': {'X': 0 + bonus_score['A'], 'Y': 3 + bonus_score['B'], 'Z': 6 + bonus_score['C']},
        # Scissors
        'C': {'X': 0 + bonus_score['B'], 'Y': 3 + bonus_score['C'], 'Z': 6 + bonus_score['A']},
    }
    return match_score[opponent][expected_output]


if __name__ == '__main__':

    with open('input.txt', encoding='utf-8') as input_file:
        score = sum(map(line_score, input_file))

    print(score)
