#!/usr/bin/python
# -*- coding: UTF-8 -*-
from functools import lru_cache


@lru_cache(maxsize=9)
def line_score(line: str) -> int:
    """ Return the score given obtained from the given line.

    Since there is a small and limited number of possibilities, results are
    cached.

    Results could be computed by hand beforehand and added to a map
    line -> score. But this is christmas fun.
    """
    # Bonus for the move selected
    bonus_score: dict = {'X': 1, 'Y': 2, 'Z': 3}
    # Parse players moves
    opponent: str = line[0]
    player: str = line[2]
    # Check match result
    match_score = {
        'A': {'X': 3, 'Y': 6, 'Z': 0},  # Rock
        'B': {'X': 0, 'Y': 3, 'Z': 6},  # Paper
        'C': {'X': 6, 'Y': 0, 'Z': 3},  # Scissors
    }
    return match_score[opponent][player] + bonus_score[player]


if __name__ == '__main__':

    with open('input.txt', encoding='utf-8') as input_file:
        score = sum(map(line_score, input_file))

    print(score)
