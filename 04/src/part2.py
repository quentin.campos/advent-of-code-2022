import click


def is_overlap(slice1, slice2):
    """ Indicates if the slice 1 overlaps slice 1.

    Slices are string in format X-Y where X and Y are numbers.
    """
    a1, b1 = map(int, slice1.split("-"))
    a2, b2 = map(int, slice2.split("-"))

    assert(a1 <= b1)
    assert(a2 <= b2)

    return (a1 <= a2 and b1 >= a2) or (a1 >= a2 and a1 <= b2)


@click.command()
@click.argument('input_file', type=click.Path(exists=True))
def main(input_file: str):
    # The score to compute
    score = 0

    with open(input_file, encoding="utf-8") as input_file:
        for line in (tline.rstrip() for tline in input_file):
            r1, r2 = line.split(",")
            if is_overlap(r1, r2):
                score += 1

    print(score)


if __name__ == '__main__':
    main()
